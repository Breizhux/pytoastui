#!/usr/bin/env python3
#! -*- coding : utf-8 -*-

import setuptools

setuptools.setup(
    name = 'pytoastui',
    version = '1.0.0',
    description = 'Une interface simple pour rédiger des articles en markdown avec un éditeur WYSIWYG',
	license = "WTFPL",
    url = 'https://gitlab.com/breizhux/pyotastui',
    author = 'Breizhux',
    author_email = 'xavier.lanne@gmx.fr',
    packages = setuptools.find_packages(
        exclude = [
            'bin',
            'lib',
            '__pycache__',
            'pyvenv.cfg',
            '.gitignore',
            'requirements.txt',
            'run_in_term.py'
        ],
    ),
    package_data = {'pytoastui' : [
        'templates/*',
    ]},
    include_package_data=True,
    install_requires = [
        'flask',
        'genipy @ git+https://gitlab.com/Breizhux/genipy.git',
    ],
	python_requires = ">=3.8",
    entry_points = {
        'console_scripts': [
            'pytoastui = pytoastui.app:main',
        ],
    },
)
