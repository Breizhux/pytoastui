#!/usr/bin/env python3
#! -*- coding : utf-8 -*-

import os
import sys
import genipy
from multiprocessing import Manager, Process
from multiprocessing.managers import BaseManager
from flask import Flask, render_template, request, redirect, url_for, jsonify

import gi
gi.require_version('Gtk', '3.0')
gi.require_version('WebKit2', '4.0')
from gi.repository import Gtk, WebKit2



class CurrentMarkdown:
    def __init__(self):
        self.__path = None
        self.__content = ""
        self.__change = False

    def is_saved(self) :
        """ Return if the current content is saved in a file."""
        return self.__change

    def get_content(self):
        """ Return the current markdown content."""
        return self.__content

    def update(self, text):
        """ When toast-ui markdown content change, this function is called."""
        self.__change = True
        self.__content = text

    def save(self):
        """ Save the current content in a file. Ask a path if needed."""
        self.__change = False
        if self.__path is None :
            self.__path = genipy.GenipyFileSelection("save", title="Sélectionner le fichier de destination").exec()
            if not os.path.exists(os.path.dirname(self.__path)) :
                self.__path = None
                return
        with open(self.__path, 'w') as file :
            file.write(self.__content)

    def load(self, path) :
        """ Load an existing markdown file."""
        #TODO : if a file already open, check if is saved before load newest
        self.__path = path
        self.__change = False
        with open(self.__path, 'r') as file :
            self.__content = file.read()


class FlaskServer:
    def __init__(self, markdown):
        self.app = Flask(__name__)
        self.markdown = markdown
        self.__process = None

        @self.app.route('/')
        def index():
            return render_template('index.html', markdown_content=self.markdown)

        @self.app.route('/save', methods=['POST'])
        def save():
            self.markdown.save()
            return redirect("/")
            return jsonify({'status': 'OK'})

        @self.app.route('/signal', methods=['POST'])
        def signal():
            content = request.get_json().get('content')
            self.markdown.update(content)
            return jsonify({'status': 'OK'})

    def start(self):
        self.app.run()


class WebViewWindow(Gtk.Window):
    def __init__(self, markdown, server):
        Gtk.Window.__init__(self)
        self.markdown = markdown
        self.server = server

        self.set_title("PyToastUi")
        self.maximize()

        self.web_view = WebKit2.WebView()
        self.web_view.load_uri("http://127.0.0.1:5000")
        self.add(self.web_view)

        self.connect('destroy', self.stop_server)

    def stop_server(self, *args):
        if self.markdown.is_saved() :
            question = genipy.GenipyQuestion(title="Enregistrer", text="Le fichier a été modifié depuis.\nVoulez-vous enregistrer les changements ?")
            if question.exec() :
                self.markdown.save()
        self.server.terminate()
        self.server.join()
        Gtk.main_quit()



def main() :
    #get the path user want to open.
    path = ""
    if len(sys.argv) > 1 :
        path = sys.argv[1]
        if not os.path.exists(path) :
            error = genipy.GenipyError(title="Chemin inexistant", text=f"Le chemin suivant n'existe pas :\n{path}")
            error.exec()
            sys.exit(1)

    ## create a manager to share object between the two process (gtk and flask)
    #add CurrentMarkdown as sharable class
    BaseManager.register('CurrentMarkdown', CurrentMarkdown)
    #open the share space manager
    with BaseManager() as manager:
        #create a sharable instance of CurrentMarkdown
        shared_markdown = manager.CurrentMarkdown()
        #load *path* if set
        if path : shared_markdown.load(path)
        #create my flask server
        flask_server = FlaskServer(shared_markdown)
        #start flask server from process
        server_process = Process(target=flask_server.start)
        server_process.start()
        #create my window
        win = WebViewWindow(shared_markdown, server_process)
        win.show_all()
        Gtk.main()

if __name__ == '__main__':
    main()
