# PyToastUi

**<span style="color: #120fc9">PyToastUI : Un éditeur markdown WYSIWYG !</span>**

Ceci est un POC (Proof Of Concept) pour montrer qu'**il est tout à fait possible de faire un éditeur markdown accessible à tous**.
Il s'agit d'un ***éditeur markdown WYSIWYG*** : ce que vous écrivez ressemble directement au résultat final (comme libreoffice).
Ainsi, les # en début de titre, les "\*" n'apparaissent pas quand vous tapez du texte en gras. C'est tout simplement facile à utiliser : intuitif.
C'est accessible à toute personne qui ne connait rien à la programmation et qui veut rédiger simplement des articles dans un format standard répandue.

![Captur of pytoastui](https://gitlab.com/Breizhux/pytoastui/-/raw/main/captur_of_pytoastui.png?ref_type=heads)
*(Oui, ce markdown a été entièrement écrit avec pytoastui).*

## Installation

**Avec pip :**

```bash
#system dependency
sudo apt install -y python3-venv python3-gi git

#create your python environment in your favorite directory
virtualenv ./
source bin/activate

#install pytoastui
pip install git+https://gitlab.com/Breizhux/pytoastui.git
ln -s /lib/python3/dist-packages/gi/ ./lib/python3.11/site-packages/

#to run pytoastui
pytoastui
#or
pytoastui /path/to/file.md
```

**Installation de l'icône pour démarrer en un clic :**

```bash
#in your pytoastui installation directory
wget https://gitlab.com/Breizhux/pytoastui/-/raw/main/run.sh?ref_type=heads -O run.sh
chmod +x run.sh
wget https://gitlab.com/Breizhux/pytoastui/-/raw/main/pytoastui.desktop?ref_type=heads -O $HOME/.local/share/applications/pytoastui.desktop
sed -i "s|pytoastui_install_dir|$PWD|g" $HOME/.local/share/applications/pytoastui.desktop
sed -i "s|pytoastui_install_dir|$PWD|g" run.sh
```